import {Routes} from "@angular/router";

export const FULL_LAYOUT_ROUTES: Routes = [
  {
    path: 'error',
    loadChildren: () => import('../../pages/full-layout/error/error.module').then(m => m.ErrorModule)
  },
  {
    path: 'register',
    loadChildren: () => import('../../pages/full-layout/register/register.module').then(m => m.RegisterModule)
  }
];
