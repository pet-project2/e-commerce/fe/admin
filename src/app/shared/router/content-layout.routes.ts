import {Routes} from "@angular/router";
import {AuthGuard} from "../security/auth-guard.service";

export const CONTENT_LAYOUT_ROUTES: Routes = [
  {
    path: 'dashboard',
    loadChildren: () => import('../../pages/content-layout/dashboard/dashboard.module').then(m => m.DashboardModule),
    canActivate: [AuthGuard]
  }
]
