import {ModuleWithProviders, NgModule, Optional, SkipSelf} from '@angular/core';
import {AuthModule} from "./security/auth.module";


@NgModule({
  imports: [
    AuthModule
  ]
})
export class SharedModule {
  static forRoot(): ModuleWithProviders<SharedModule> {
    return {
      ngModule: SharedModule,
    }
  }

  constructor(@Optional() @SkipSelf() parentModule: SharedModule) {
    if (parentModule) {
      throw new Error('SharedModule is already loaded. Import it in the AppModule only');
    }
  }
}
