import { NgModule } from '@angular/core';
import {OAuthModule} from "angular-oauth2-oidc";
import {AuthService} from "./auth.service";
import {HttpClientModule} from "@angular/common/http";

@NgModule({
  declarations: [],
  imports: [
    HttpClientModule,
    OAuthModule.forRoot()
  ],
  providers: [AuthService]
})
export class AuthModule { }
