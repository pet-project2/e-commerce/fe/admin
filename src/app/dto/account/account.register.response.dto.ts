export class AccountRegisterResponseDto {
  public isSuccess: boolean;

  public message: string;

  public statusCode: number;

  constructor(isSuccess: boolean, message: string, statusCode: number) {
    this.isSuccess = isSuccess;
    this.message = message;
    this.statusCode = statusCode;
  }
}
