import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {FormControl, FormGroup, Validators} from "@angular/forms";
import {AccountRegisterDto} from "../../../dto/account/account.register.dto";
import {AccountService} from "../../../services/account/account.service";
import {HttpStatusCode} from "@angular/common/http";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  constructor(private accountService: AccountService) { }

  ngOnInit(): void {
    this.loadForm();
  }

  public loadForm(): void {
    this.registerForm = new FormGroup({
      firstName: new FormControl(''),
      lastName: new FormControl(''),
      email: new FormControl('', [Validators.required, Validators.email]),
      password: new FormControl('', [Validators.required]),
      passwordConfirm: new FormControl()
    })
  }

  public registerUser(): Promise<void> {
    const form = this.registerForm.value;
    const account: AccountRegisterDto = {
      firstName: form.firstName,
      lastName: form.lastName,
      email: form.email,
      password: form.password,
      confirmPassword: form.confirmPassword
    };

    this.accountService.registerAccount(account).subscribe(response => {
      if(response.isSuccess && response.statusCode == HttpStatusCode.Ok) {
        console.log("Register new user is successfully");
        return;
      }
      console.log("Register new user failed");
      console.log(response.message);
    });

    return  Promise.resolve();
  }
}
