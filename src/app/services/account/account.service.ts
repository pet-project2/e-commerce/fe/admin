import {Injectable} from '@angular/core';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {AccountRegisterDto} from "../../dto/account/account.register.dto";
import {Observable} from "rxjs";
import {AccountRegisterResponseDto} from "../../dto/account/account.register.response.dto";

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  baseUrl = environment.apiUrl + 'account';

  constructor(private http: HttpClient) {
  }

  public registerAccount(user: AccountRegisterDto): Observable<AccountRegisterResponseDto> {
    return this.http.post<AccountRegisterResponseDto>(this.baseUrl + "/register", user);
  }
}
