import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ContentLayoutComponent} from "./layouts/content-layout/content-layout.component";
import {CONTENT_LAYOUT_ROUTES} from "./shared/router/content-layout.routes";
import {FullLayoutComponent} from "./layouts/full-layout/full-layout.component";
import {FULL_LAYOUT_ROUTES} from "./shared/router/full-layout.routes";

const routes: Routes = [
  {
    path: '', redirectTo: 'register', pathMatch: 'full'
  },
  {
    path: '', component: FullLayoutComponent, children: FULL_LAYOUT_ROUTES
  },
  {
    path: 'admin', component: ContentLayoutComponent, children: CONTENT_LAYOUT_ROUTES
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
